import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Norm {

    static String dictionary = "abcdefghijklmnopqrstuvwxyz1234567890.,";
    static Double multiplier = 0.025;


    public static NormResult norm(String input) {
        List<Character> characters = input.chars().mapToObj(e -> (char) e).collect(Collectors.toList());
        return norm(characters);
    }

    public static NormResult norm(List<?> input) {

        Double max = Double.MIN_VALUE;
        Double min = Double.MAX_VALUE;
        for (Object n : input) {
            Double i = toDouble(n);
            if (i < min) min = i;
            if (i > max) max = i;
        }
        List<Double> output = new ArrayList<>();
        for (Object n : input) {
            Double i = toDouble(n);
            output.add(normalizeSingle(min, max, i));
        }
        return new NormResult(min, max, output);
    }

    public static Double toDouble(Object n) {
        if (n instanceof Double) {
            return (Double) n;
        } else if (n instanceof Integer) {
            return ((int) n) * 1.0;
        } else if (n instanceof Character) {
            Character c = (Character) n;
            if (Character.isLetter(c) && Character.isUpperCase(c)) {
                c = Character.toLowerCase(c);
            }
            Integer pos = dictionary.indexOf(c);
            System.out.println(pos);
            if (pos != -1) {
                return pos * multiplier;
            } else {
                return 0.0;
            }
        }
        return Double.MIN_VALUE;
    }

    public static String denormalizeString(Double min, Double max, List<Double> result) {
        StringBuilder builder = new StringBuilder();
        for (Double r:result) {
            Integer pos = Math.toIntExact(Math.round(demoralizeSingle(min, max, r) / multiplier));
            try {
                char c = dictionary.charAt(pos);
                builder.append(c);
            } catch (IndexOutOfBoundsException e) {

            }
        }
        return builder.toString();
    }

    public static double normalizeSingle( double min, double max, double input) {
        return (input - min) / (max - min) * 0.9 + 0.1;
    }

    public static double demoralizeSingle( double min, double max, double input) {
        return min + (input - 0.1) * (max - min) / 0.9;
    }


    public static class NormResult {
        Double min;
        Double max;
        List<Double> result;

        public NormResult(Double min, Double max, List<Double> result) {
            this.min = min;
            this.max = max;
            this.result = result;
        }

        public Double getMin() {
            return min;
        }

        public Double getMax() {
            return max;
        }

        public List<Double> getResult() {
            return result;
        }
    }


}
