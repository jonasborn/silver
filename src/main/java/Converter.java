public class Converter {


    String code;
    Integer morySize = 65535;
    private int ident;

    public Converter(String code) {
        this.code = code;
    }

    public Converter(String code, Integer morySize) {
        this.code = code;
        this.morySize = morySize;
    }

    public static char getChar(int i) {
        i--;
        return i < 0 || i > 25 ? '?' : (char) ('a' + i);
    }

    public String convert() {
        StringBuilder builder = new StringBuilder();
        header(builder);
        Integer counter = 0;
        for (String single : code.split("@")) {
            if (counter == 0) {
                main(single, builder);
            } else {
                function(new String(new char[]{getChar(counter)}), single, builder);
            }
            counter++;
        }
        footer(builder);
        String res = builder.toString();


        return res;
    }

    private void header(StringBuilder builder) {

        builder.append("import java.util.ArrayList;\n");
        builder.append("import java.util.Iterator;\n");
        builder.append("import java.util.List;\n");

        builder.append("public class ConvertedScript {\n");
        builder.append("private int p = 0;\n");
        builder.append("private int[] m = new int[").append(morySize).append("];\n");
        builder.append("private Iterator<Integer> input;\n");
        builder.append("private List<Integer> output = new ArrayList<>();\n");
    }

    private void footer(StringBuilder builder) {
        builder.append("}\n");
    }

    private void convert(String code, StringBuilder source) {
        for (int i = 0; i < code.length(); i++) {

            char target = code.charAt(i);


            switch (target) {
                case '>':
                    source.append("p++;\n");
                    break;
                case '<':
                    source.append("p--;\n");
                    break;
                case '+':
                    source.append("m[p]++;\n");
                    break;
                case '-':
                    source.append("m[p]--;\n");
                    break;
                case '.':
                    source.append("output.add(m[p]);\n");
                    break;
                case ',':
                    source.append("m[p] = input.next();\n");
                    break;
                case '[':
                    source.append("while(m[p] != 0) {\n");
                    ident++;
                    break;
                case ']':
                    source.append("}\n");
                    break;
                case '$':
                    source.append("p = m[p];\n");
                    break;
                case '!':
                    source.append("m[p] = p;\n");
                    break;
                default: {
                    if (Character.isLetter(target) && Character.isLowerCase(target)) {
                        source.append(target).append("();");
                    } else {
                        continue;
                    }
                }
            }
            if (i + 1 < code.length() && code.charAt(i + 1) == ']') ident--;
        }
    }

    private void main(String code, StringBuilder builder) {
        builder.append("public List<Integer> main(List<Integer> args) {\n");
        builder.append("this.input = args.iterator();\n");
        convert(code, builder);
        builder.append("return output;\n");
        builder.append("}\n");
    }

    private void function(String name, String code, StringBuilder builder) {

        builder.append("private void ");
        builder.append(name);
        builder.append("() {\n");
        builder.append("int[] m = new int[").append(morySize).append("];\n");
        convert(code, builder);
        builder.append("}\n");

    }

}
