public class GPU {

    public static int[] fromBrainfuck(String s) {
        String dict = "><+-.,[]";
        int[] res = new int[s.length()];
        int i = 0;
        for (char c : s.toCharArray()) {
            res[i] = dict.indexOf(c) + 1;
            i++;
        }
        return res;
    }


    public static void main(String[] _args) {


        //ARRAY MUST BE MULTIPLE OF 8
        final int size = 8;

        final int[] a = new int[]{3, 3, 3, 3, 3, 3, 3, 5};


        //CPointer, DPointer, MemPointer
        final int[] pointer = {0, 0, 0};

        final int[] mem = new int[2048];

        final int[] sum = new int[64];

        int[] bfk = fromBrainfuck("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.");
        int[] bfk2 = fromBrainfuck("+++++++.");

        for (int i = 0; i < bfk.length; i++) {
            System.out.print(bfk[i]);
        }
        System.out.println();

        final int[][] commands = new int[][]{bfk, bfk2};


        KernelInterpreter interpreter = new KernelInterpreter(commands );
        interpreter.start();




    }

    private int[] push(int[] source, int value) {
        int[] res = new int[source.length + 1];
        for (int i = 0; i < source.length; i++) {
            res[i] = source[i];
        }
        res[source.length] = value;
        return res;
    }

    public static int[] interpret(int[] code) {

        final int[] res = new int[32];
        final int[] mem = new int[65535];
        final int[] pointer = new int[]{0, 0, 0, 0};

        while (pointer[0] < code.length) {
            int current = code[pointer[0]];

            switch (current) {
                case 1:
                    pointer[1] = (pointer[1] == mem.length - 1) ? 0 : pointer[1] + 1;
                    break;
                case 2:
                    pointer[1] = (pointer[1] == 0) ? mem.length - 1 : pointer[1] - 1;
                    break;
                case 3:
                    mem[pointer[1]]++;
                    break;
                case 4:
                    mem[pointer[1]]--;
                    break;
                case 5:
                    res[pointer[3]] = mem[pointer[1]];
                    pointer[3]++;
                    break;
                case 6: //mem[pointer[1]] = (byte) sc.next().charAt(0);
                    break;
                case 7: {
                    if (mem[pointer[1]] == 0) {
                        pointer[0]++;
                        while (pointer[2] > 0 || code[pointer[0]] != 8) {
                            if (code[pointer[0]] == 7) pointer[2]++;
                            if (code[pointer[0]] == 8) pointer[2]--;
                            pointer[0]++;
                        }
                    }
                    break;
                }
                case 8: {
                    if (mem[pointer[1]] != 0) {
                        pointer[0]--;
                        while (pointer[2] > 0 || code[pointer[0]] != 7) {
                            if (code[pointer[0]] == 8) pointer[2]++;
                            if (code[pointer[0]] == 7) pointer[2]--;
                            pointer[0]--;
                        }
                        pointer[0]--;
                    }
                    break;
                }
            }

            pointer[0]++;

        }

        int[] output = new int[pointer[3]];

        for (int i = 0; i < pointer[3]; i++) {
            output[i] = res[i];
        }

        return output;


    }

}
