import com.aparapi.Kernel;
import com.aparapi.Range;

public class KernelInterpreter {

    int[] code;
    int[] codeLengths;

    int[] mem;
    int[] memLengths;

    int[] output;
    int[] outLengths;



    public KernelInterpreter(int[][] inputs) {

        code = new int[0];
        for (int[] input:inputs) {
            code =     push(code, input);
        }

        codeLengths = new int[inputs.length];
        memLengths = new int[inputs.length];
        outLengths = new int[inputs.length];

        for (int i = 0; i < codeLengths.length; i++) {
            codeLengths[i] = inputs[i].length;
        }

        for (int i = 0; i < memLengths.length; i++) {
            memLengths[i] = 65535;
        }

        for (int i = 0; i < outLengths.length; i++) {
            outLengths[i] = 65535;
        }



    }


    public void start() {

        Kernel kernel = new Kernel(){
            @Override public void run() {
                int gid = getGlobalId();

                int codeStart = 0;
                for (int i = 0; i < gid; i++) {
                    codeStart = codeStart + codeLengths[i];
                }
                int codeEnd = codeStart + codeLengths[gid];

                int pointer = codeStart;

                int memStart = 0;
                for (int i = 0; i < gid; i++) {
                    memStart = memStart + memLengths[i];
                }
                int memEnd = memStart + memLengths[gid];


                int outStart = 0;
                for (int i = 0; i < gid; i++) {
                    outStart = outStart + outLengths[gid];
                }
                int outEnd = outStart + outLengths[gid];

                while (pointer + codeStart < codeEnd) {

                    switch (code[codeStart + pointer]) {
                        case 1:
                            pointer = pointer + 1;
                            break;
                        case 2:
                            pointer = pointer - 1;
                            break;
                        case 3:
                            mem[memStart + pointer]++;
                            break;
                        case 4:
                            mem[memStart + pointer]--;
                            break;

                }

            }

        }};

        kernel.execute(Range.create(codeLengths.length));

        System.out.println(kernel.isRunningCL());

        System.out.println(kernel.getTargetDevice().getType());
        kernel.dispose();

    }


    private int[] interpret(int[] code) {

        final int[] res = new int[32];
        final int[] mem = new int[65535];
        final int[] pointer = new int[]{0, 0, 0, 0};

        while (pointer[0] < code.length) {
            int current = code[pointer[0]];

            switch (current) {
                case 1:
                    pointer[1] = (pointer[1] == mem.length - 1) ? 0 : pointer[1] + 1;
                    break;
                case 2:
                    pointer[1] = (pointer[1] == 0) ? mem.length - 1 : pointer[1] - 1;
                    break;
                case 3:
                    mem[pointer[1]]++;
                    break;
                case 4:
                    mem[pointer[1]]--;
                    break;
                case 5:
                    res[pointer[3]] = mem[pointer[1]];
                    pointer[3]++;
                    break;
                case 6: //mem[pointer[1]] = (byte) sc.next().charAt(0);
                    break;
                case 7: {
                    if (mem[pointer[1]] == 0) {
                        pointer[0]++;
                        while (pointer[2] > 0 || code[pointer[0]] != 8) {
                            if (code[pointer[0]] == 7) pointer[2]++;
                            if (code[pointer[0]] == 8) pointer[2]--;
                            pointer[0]++;
                        }
                    }
                    break;
                }
                case 8: {
                    if (mem[pointer[1]] != 0) {
                        pointer[0]--;
                        while (pointer[2] > 0 || code[pointer[0]] != 7) {
                            if (code[pointer[0]] == 8) pointer[2]++;
                            if (code[pointer[0]] == 7) pointer[2]--;
                            pointer[0]--;
                        }
                        pointer[0]--;
                    }
                    break;
                }
            }

            pointer[0]++;

        }

        int[] output = new int[pointer[3]];

        for (int i = 0; i < pointer[3]; i++) {
            output[i] = res[i];
        }

        return output;
    }

    private static int[] push(int[] array, int[] push) {
        int[] longer = new int[array.length + push.length];

        System.arraycopy(array, 0, longer, 0, array.length);
        System.arraycopy(push, 0, longer, array.length, push.length);
        return longer;
    }
}
