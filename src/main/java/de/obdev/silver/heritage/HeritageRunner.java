package de.obdev.silver.heritage;


import de.obdev.silver.heritage.checker.SimpleChecker;
import de.obdev.silver.heritage.io.IOElement;
import de.obdev.silver.heritage.io.IOManager;
import de.obdev.silver.heritage.mutator.Creator;
import de.obdev.silver.heritage.mutator.SimpleCreator;
import de.obdev.silver.heritage.mutator.CreatorPool;
import de.obdev.silver.heritage.runner.Result;
import de.obdev.silver.heritage.runner.ScriptRunner;

import java.util.ArrayList;
import java.util.List;

public class HeritageRunner {

    public static void main(String[] args) throws Exception {

        for (int i = 0; i < 40; i++) {
            Thread.sleep(4000);
            run();
        }



    }

    public static void run() throws Exception {

        //HeritagePool.rate(new WorkingHertiage(), 1.0);

        Creator creator = CreatorPool.getBest();
        List<Heritage> heritages = creator.create();

        for (int i = 0; i < heritages.size(); i++) {
            System.out.println(heritages.get(i).getPrintableCode());
        }

        ScriptRunner runner = new ScriptRunner();

        IOManager.add(new IOElement(new int[0], codepoints("Hallo World")));

        List<IOElement> ioes = IOManager.create(creator.getSize());


        List<Result> results = runner.run(ioes, heritages);


        SimpleChecker checker = new SimpleChecker();

        checker.check(results);

    }

    public static int[] codepoints(String str) {
        int[] arr = new int[str.length()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = str.charAt(i);
        }
        return arr;
    }

}
