package de.obdev.silver.heritage.mutator;

import de.obdev.silver.heritage.Heritage;

import java.util.List;

public interface Creator {

    public List<Heritage> create() throws Exception;

    public void mutate();

    public int getSize();

}
