package de.obdev.silver.heritage.mutator;

import de.obdev.silver.heritage.Heritage;
import de.obdev.silver.heritage.HeritagePool;
import de.obdev.silver.util.Random;

import java.util.ArrayList;
import java.util.List;

public class SimpleCreator implements Creator {

    static Integer size;

    double chance;

    double mutated;
    double crossed;
    double normal;

    public SimpleCreator() {
        size = Random.rand(0,100);
        chance = Random.rand(0.0, 1.0);
        mutated = Random.rand(0.0, 1.0);
        crossed = Random.rand(0.0, 1.0);
        normal = Random.rand(0.0, 1.0);
    }



    @Override
    public List<Heritage> create() throws Exception {
        List<Heritage> heritages = new ArrayList<>();
        while (heritages.size() < size) {
            if (Random.decide(mutated)) heritages.add(HeritagePool.next().setLastCreator(this).mutate());

            if (Random.decide(crossed)) {
                Heritage f = HeritagePool.next();
                Heritage s = HeritagePool.next();
                heritages.add(f.cross(s).setLastCreator(this));
            }
            if (Random.decide(normal)) heritages.add(HeritagePool.next().setLastCreator(this));
        }
        while (heritages.size()>size) heritages.remove(0);

        //TODO ADD CHECKUP FOR UNNEEDED LOOPS
        return heritages;
    }

    @Override
    public void mutate() {
        size = Random.rand(0,512);
        chance = Random.rand(0.0, 1.0);
        mutated = Random.rand(0.0, 1.0);
        crossed = Random.rand(0.0, 1.0);
        normal = Random.rand(0.0, 1.0);
    }

    @Override
    public int getSize() {
        return size;
    }
}
