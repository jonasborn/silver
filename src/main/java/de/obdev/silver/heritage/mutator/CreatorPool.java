package de.obdev.silver.heritage.mutator;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import de.obdev.silver.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

public class CreatorPool implements Runnable {

    static Logger logger = LogManager.getLogger();

    static Integer max = 20;

    static Callable<Creator> defaultCreator = SimpleCreator::new;

    static Multimap<Double, Creator> storage = MultimapBuilder.treeKeys().arrayListValues().build();


    static {
        new Thread(new CreatorPool()).start();
    }

    private CreatorPool() {

    }

    public static Creator getBest() throws InterruptedException {
        Optional<Map.Entry<Double, Creator>> optional = storage.entries().stream().findFirst();
        if (optional.isPresent()) {
            return optional.get().getValue();
        } else {
            Creator creator = null;
            try {
                creator = defaultCreator.call();
                storage.put(Random.rand(0.0, Double.MAX_VALUE), creator);
                return creator;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        logger.warn("Unable to create new default creator, using {}", SimpleCreator.class);
        return new SimpleCreator();
    }


    public static void rate(Creator creator, Double fitness) {
        Optional<Map.Entry<Double, Creator>> optional = storage.entries().stream().filter((e) ->
                e.getValue().equals(creator)).findAny();
        if (optional.isPresent()) {
            storage.values().removeIf(e -> e.equals(creator));
            storage.put(optional.get().getKey() - fitness, creator);
        } else {
            storage.put(fitness, creator);
        }
    }


    @Override
    public void run() {
        while (true) {
            logger.info("Started scheduler for CreatorPool");
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Iterator<Creator> it = storage.values().iterator();
            int p = 0;
            while (it.hasNext()) {
                if (max > 5 && p > max -5) {
                    it.remove();
                }
            }
            logger.info("Creating {} new creators", max - storage.size());
            while (storage.size() < max) {
                try {
                    Creator creator;
                    if (Random.decide(0.5)) {
                        creator = CreatorPool.getBest();
                        creator.mutate();
                    } else {
                        creator = defaultCreator.call();
                    }
                    System.out.println("new");
                    storage.put(Random.rand(0.0, Double.MAX_VALUE), creator);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
