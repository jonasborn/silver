package de.obdev.silver.heritage.runner;

import de.obdev.silver.heritage.Heritage;
import de.obdev.silver.heritage.WorkingHertiage;
import de.obdev.silver.heritage.io.IOElement;

import java.util.ArrayList;
import java.util.List;

public class OpenCLRunner {

    public static void main(String[] args) {
        int[][] ins = new int[0][0];

        int[] bfk = new WorkingHertiage().getCode();

        int[][] codes = new int[3][bfk.length];
        codes[0] = bfk;
        codes[1] = bfk;
        codes[2] = bfk;

        List<Heritage> heritages = new ArrayList<>();
        List<IOElement> ioes = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            heritages.add(new WorkingHertiage());
            ioes.add(new IOElement(new int[0], new int[0]));
        }

        List<Result> results = new ScriptRunner().runNew(ioes, heritages);
        for (int i = 0; i < results.size(); i++) {
            System.out.println(results.get(i).getResult().length);
        }
    }

    public static void run(List<IOElement> ioes, List<Heritage> heritages) {

    }

}
