package de.obdev.silver.heritage.runner;

import de.obdev.silver.heritage.Heritage;
import de.obdev.silver.heritage.io.IOElement;

import java.util.List;

public interface Runner {

    List<Result> run(List<IOElement> ioes, List<Heritage> heritage);

}
