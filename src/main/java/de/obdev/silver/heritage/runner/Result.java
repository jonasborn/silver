package de.obdev.silver.heritage.runner;

import de.obdev.silver.heritage.Heritage;
import de.obdev.silver.heritage.io.IOElement;

public class Result {

    Heritage heritage;

    IOElement ioe;
    int[] code;
    int[] result;
    int stepps;

    public Result(Heritage heritage, IOElement ioe, int[] code, int[] result, int stepps) {
        this.heritage = heritage;
        this.ioe = ioe;
        this.code = code;
        this.result = result;
        this.stepps = stepps;
    }

    public Heritage getHeritage() {
        return heritage;
    }

    public IOElement getIoe() {
        return ioe;
    }

    public int[] getCode() {
        return code;
    }

    public int[] getResult() {
        return result;
    }

    public int getStepps() {
        return stepps;
    }
}
