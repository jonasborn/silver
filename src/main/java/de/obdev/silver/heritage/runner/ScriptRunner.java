package de.obdev.silver.heritage.runner;

import com.aparapi.Kernel;
import de.obdev.silver.heritage.Heritage;
import de.obdev.silver.heritage.io.IOElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScriptRunner implements Runner {

    static int maxStepps = 500;

    static Logger logger = LogManager.getLogger();

    @Override
    public List<Result> run(List<IOElement> ioes, List<Heritage> heritages) {

        return runNew(ioes, heritages);

//        int maxCodeLength = Integer.MIN_VALUE;
//        List<int[]> codes = new ArrayList<>();
//        for (Heritage heritage : heritages) {
//            int[] code = heritage.getCode();
//            if (code.length > maxCodeLength) maxCodeLength = code.length;
//            codes.add(code);
//        }
//
//        int[][] finalCodes = new int[heritages.size()][maxCodeLength];
//        for (int i = 0; i < codes.size(); i++) {
//            finalCodes[i] = fill(codes.get(i), maxCodeLength);
//        }
//
//
//        int maxInputLength = Integer.MAX_VALUE;
//        for (IOElement ioe : ioes) {
//            if (ioe.getInput().length < maxInputLength) maxInputLength = ioe.getInput().length;
//        }
//
//        int[][] finalInputs = new int[ioes.size()][];
//        for (int i = 0; i < ioes.size(); i++) {
//            finalInputs[i] = fill(ioes.get(i).getInput(), maxInputLength);
//        }
//
//
//        RawResult rawResult = run(finalInputs, finalCodes, maxStepps);
//
//        if (rawResult.isValid()) {
//            logger.info("Successfully accomplished {} scripts with readable results", rawResult.codes.length);
//        } else {
//            logger.error("Received incomplete data from kernel function. This invalidates the whole generation!");
//            return null;
//        }
//
//        List<Result> results = new ArrayList<>();
//
//        for (int i = 0; i < rawResult.codes.length; i++) {
//            IOElement ioe = (ioes.size() > i) ? ioes.get(i) : null;
//            Result result = new Result(
//                    heritages.get(i),
//                    ioe,
//                    rawResult.codes[i],
//                    rawResult.results[i],
//                    rawResult.stepps[i]
//            );
//            results.add(result);
//        }
//
//
//
//        return results;


    }

    public List<Result> runNew(List<IOElement> ioes, List<Heritage> heritages) {
        int[][] inputs = new int[ioes.size()][];
        for (int i = 0; i < ioes.size(); i++) {
            inputs[i] = ioes.get(i).getOutput();
        }

        int[][] codes = new int[heritages.size()][];
        for (int i = 0; i < heritages.size(); i++) {
            codes[i] = heritages.get(i).getCode();
        }

        RawResult rr = run(inputs, codes, 5000);
        List<Result> results = new ArrayList<>();
        for (int i = 0; i < rr.getResults().length; i++) {
            results.add(new Result(
                    heritages.get(i),
                    ioes.get(i),
                    codes[i],
                    rr.getResults()[i],
                    rr.getStepps()[i])
            );
            heritages.get(i).setLastResult(rr.getResults()[i]);
        }
        return results;
    }



    // APARAPI is NOT ABLE TO handle multi-dimensional arrays
    // APARAPI is NOT ABLE TO handle switches
    // APARAPI is NOT ABLE TO run parallel instances

    public static RawResult run(int[][] inputs, final int[][] codes, final int maxStepps) {

        int[] tempInput = new int[0];
        for (int[] input : inputs) { //TODO FIX
            tempInput = push(tempInput, input);
        }

        final int[] in = tempInput;

        int[] tempCode = new int[0];
        for (int[] input : codes) {
            tempCode = push(tempCode, input);
        }

        final int[] code = tempCode;

        int[] inputLengths = new int[codes.length];
        int[] codeLengths = new int[codes.length];
        int[] memLengths = new int[codes.length];
        int[] outLengths = new int[codes.length];

        int[] stepps = new int[codes.length];

        final int[] pointer = new int[codes.length];
        final int[] position = new int[codes.length];
        final int[] loopPointer = new int[codes.length];
        final int[] outPointer = new int[code.length];


        for (int i = 0; i < codes.length; i++) {
            inputLengths[i] = 0;
            codeLengths[i] = codes[i].length;
            outLengths[i] = 65535;
            memLengths[i] = 65535;
            pointer[i] = 0;
            position[i] = 0;
            loopPointer[i] = 0;
            outPointer[i] = 0;
            stepps[i] = 0;
        }


        final int[] mem = new int[codeLengths.length * 65535];
        final int[] out = new int[codeLengths.length * 65535];


        for (int i = 0; i < codeLengths.length; i++) {
            outLengths[i] = 65535;
            pointer[i] = 0;
            position[i] = 0;
            loopPointer[i] = 0;
            outPointer[i] = 0;
        }


        Kernel kernel = new Kernel() {
            @Override
            public void run() {
                int gid = getGlobalId();

                if (gid < codeLengths.length) {
                    if (stepps[gid] < maxStepps) {
                        int inputStart = 0;
                        for (int i = 0; i < gid; i++) {
                             inputStart = inputStart + inputLengths[i];
                        }
                        int inputEnd = inputStart + inputLengths[gid];

                        int codeStart = 0;
                        for (int i = 0; i < gid; i++) {
                            codeStart = codeStart + codeLengths[i];
                        }
                        int codeEnd = codeStart + codeLengths[gid];

                        int memStart = 0;
                        for (int i = 0; i < gid; i++) {
                            memStart = memStart + memLengths[i];
                        }
                        int memEnd = memStart + memLengths[gid];

                        int outStart = 0;
                        for (int i = 0; i < gid; i++) {
                            outStart = outStart + outLengths[i];
                        }

                        int outEnd = outStart + outLengths[gid];

                        outPointer[gid] = outStart;

                        position[gid] = codeStart;
                        //position[gid] < codeEnd && position[gid] >= codeEnd && position[gid] < 0 &&
                        while (position[gid] < codeEnd && stepps[gid] < maxStepps) {
                            int current = code[position[gid]];


                            if (current == 1) {
                                pointer[gid] = ((memStart + pointer[gid] + 1) < memEnd - 1) ? pointer[gid] + 1 : 0;
                                //pointer[gid] = pointer[gid] + 1;
                            }
                            if (current == 2) {
                                pointer[gid] = (pointer[gid] - 1 >= 0) ? pointer[gid] - 1 : 0;
                                //pointer[gid] = pointer[gid] - 1;
                            }
                            if (current == 3) {
                                if (memStart + pointer[gid] + 1 < memEnd) mem[memStart + pointer[gid]]++;
                            }
                            if (current == 4) {
                                if (pointer[gid] > -1) mem[memStart + pointer[gid]]--;

                            }
                            if (current == 5) {
                                out[outPointer[gid]] = mem[memStart + pointer[gid]];
                                outPointer[gid] = (outPointer[gid] + 1 > outEnd) ? outStart : outPointer[gid] + 1;
                                //outPointer[gid] = outPointer[gid] + 1;
                            }
                            if (current == 6) {
                                mem[memStart + pointer[gid]] = (inputStart + pointer[gid] > inputEnd) ? 0 : in[inputStart + pointer[gid]];
                            }
                            if (current == 7) {
                                if (mem[memStart + pointer[gid]] == 0) {
                                    position[gid]++;
                                    boolean loop = false;
                                    if (loopPointer[gid] > 0) loop = true;
                                    //if (position[gid] >= codeEnd) break;
                                    if (code[position[gid]] != 8) loop = true;
                                    if (stepps[gid] > maxStepps) loop = false;
                                    while (loop && stepps[gid] < maxStepps) {
                                        if (code[position[gid]] == 7) loopPointer[gid]++;
                                        if (code[position[gid]] == 8) loopPointer[gid]--;
                                        position[gid]++;
                                        loop = false;
                                        if (position[gid] < codeEnd) {
                                            if (loopPointer[gid] > 0) loop = true;
                                            if (code[position[gid]] != 8) loop = true;
                                        }
                                        stepps[gid]++;
                                    }
                                }
                            }
                            if (current == 8) {
                                if (mem[memStart + pointer[gid]] != 0) {
                                    position[gid]--;
                                    boolean loop = false;
                                    if (loopPointer[gid] > 0) loop = true;
                                    if (code[position[gid]] != 7) loop = true;
                                    if (stepps[gid] > maxStepps) loop = false;
                                    while (loop && stepps[gid] < maxStepps) {
                                        if (code[position[gid]] == 8) loopPointer[gid]++;
                                        if (code[position[gid]] == 7) loopPointer[gid]--;
                                        position[gid]--;
                                        loop = false;
                                        if (position[gid] >= 0) {
                                            if (loopPointer[gid] > 0) loop = true;
                                            if (code[position[gid]] != 7) loop = true;
                                        }
                                        stepps[gid]++;
                                    }
                                    position[gid]--;
                                }

                            }

                            position[gid]++;
                            stepps[gid]++;
                        }
                    }
                }

            }
        };


        int rounds = codes.length;
        while ((rounds % 8) != 0) rounds++;

        try {
            kernel.execute(rounds);
            if (!kernel.isRunningCL()) {
                logger.fatal("Impossible to execute the given scripts with OpenCL, used {} instead", kernel.getTargetDevice().getType());
            }
        } catch (Exception e) {
            for (int i = 0; i < stepps.length; i++) {
                stepps[i] = maxStepps;
            }
        }
        int[][] results = new int[codes.length][];


        for (int gid = 0; gid < codes.length; gid++) {
            int outStart = 0;
            for (int i = 0; i < gid; i++) {
                outStart = outStart + outLengths[i];
            }

            try {
                results[gid] = (Arrays.copyOfRange(out, outStart, outPointer[gid]));
            } catch (Exception e) {
                logger.error("An error occurred, this invalidates the whole generation: {}", e);
            }

        }

        kernel.dispose();

        return new RawResult(inputs, codes, results, stepps);

    }

    public static class RawResult {
        private int[][] inputs;
        private int[][] codes;
        private int[][] results;
        private int[] stepps;

        public RawResult(int[][] inputs, int[][] codes, int[][] results, int[] stepps) {
            this.inputs = inputs;
            this.codes = codes;
            this.results = results;
            this.stepps = stepps;
        }

        public int[][] getInputs() {
            return inputs;
        }

        public int[][] getCodes() {
            return codes;
        }

        public int[][] getResults() {
            return results;
        }

        public int[] getStepps() {
            return stepps;
        }

        public boolean isValid() {
            int total = inputs.length + results.length + stepps.length;
            System.out.println("I" + inputs.length);
            System.out.println("R" + results.length);
            System.out.println("S" + stepps.length);
            return total == (codes.length * 3);
        }

    }


    private static int[] push(int[] array, int[] push) {
        int[] longer = new int[array.length + push.length];

        System.arraycopy(array, 0, longer, 0, array.length);
        System.arraycopy(push, 0, longer, array.length, push.length);
        return longer;
    }
}
