package de.obdev.silver.heritage;

import de.obdev.silver.util.Random;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

public class Genome implements Serializable {

    int[] commands = new int[] {1,2,3,4,5,6,7,8};

    int size = 8;

    double chance = 0.5;

    int[] code;

    public Genome(int size, double chance, int[] code) {
        this.size = size;
        this.chance = chance;
        this.code = code;
    }

    public Genome(int size, int[] code) {
        this.size = size;
        this.code = code;
        chance = Random.rand(0.0, 1.0);
    }

    public Genome(int size) {
        this.size = size;
        code = new int[size];
        rand();
    }

    public Genome() {
        code = new int[size];
        rand();
    }

    public Genome rand() {
        chance = Random.rand(0.0, 1.0);
        int last = 0;
        for (int i = 0; i < size; i++) {
            //TODO ADD ADVANCED SUPPORT!
            int s = ThreadLocalRandom.current().nextInt(0, commands.length);
            if (last == 7) {
                while (s == 8) {
                    s = ThreadLocalRandom.current().nextInt(0, commands.length);
                }
            }
            code[i] = commands[s];
            last = s;
        }
        return this;
    }

    public int[] getCode() {
        return code;
    }

    public Genome mutate() {
        chance = (Random.decide(chance)) ? Random.rand(0.0, 1.0) : chance;
        for (int i = 0; i < chance * 10; i++) {
            int s = Random.rand(0, size);
            if (Random.decide(chance)) code[s] = commands[Random.rand(0, commands.length)];
        }
        return this;
    }

    public Genome cross(Genome genome) {
        double chance = (Random.decide(0.5)) ? this.chance : genome.chance;
        int[] a = this.commands.clone();
        int[] b = genome.commands.clone();
        int size;
        int[] res;
        if (a.length<b.length) {
            for (int i = 0; i < a.length; i++) if (Random.decide(0.5)) b[i] = a[i];
            size = genome.size;
            res = b;
        } else {
            for (int i = 0; i < a.length; i++) if (Random.decide(0.5)) a[i] = b[i];
            size = this.size;
            res = a;
        }
        return new Genome(size, chance, res);

    }
}
