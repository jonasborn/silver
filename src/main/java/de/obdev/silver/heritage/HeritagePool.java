package de.obdev.silver.heritage;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.gson.reflect.TypeToken;
import de.obdev.silver.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.concurrent.CountDownLatch;

public class HeritagePool implements Runnable {

    static Logger logger = LogManager.getLogger();

    static Integer max = 20;

    static Integer storeTimeout = 5;

    static Multimap<Double, Heritage> storage = MultimapBuilder.treeKeys().arrayListValues().build();

    static Multimap<Double, Heritage> cache = MultimapBuilder.treeKeys().arrayListValues().build();

    static CountDownLatch latch = new CountDownLatch(1);

    static {
        new Thread(new HeritagePool()).start();
        try {
            Multimap<Double, Heritage> st = FileStorage.read(HeritagePool.class, new TypeToken<Multimap<Double, Heritage>>() {});
            if (st != null) {
                storage = st;
                logger.info("Loaded heritages from file system");
            }
        } catch (Exception e) {

        }
        //storage.put(1.0, new WorkingHertiage());
        latch.countDown();

    }

    private HeritagePool() {

    }


    public static Heritage next()  throws InterruptedException {
        Heritage heritage = n();
        return heritage;
    }

    public static Heritage n() throws InterruptedException {
        latch.await();

        Optional<Heritage> op = storage.values().stream().findAny();
        return op.orElseGet(Heritage::new);
        //return new WorkingHertiage();
    }

    int pos = 0;

    @Override
    public void run() {
        logger.info("Started scheduler for HeritagePool");
        while (true) {

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Manually block due issues
            latch = new CountDownLatch(1);
            storage.putAll(cache);
            cache.clear();
            Iterator<Heritage> it = storage.values().iterator();
            int p = 0;
            while (it.hasNext()) {
                it.next();
                if (p > max) {
                    it.remove();
                }
                p++;
            }

            if (pos > storeTimeout) {
                pos = 0;
                logger.info("Wrote current heritages to file system");
                Optional<Map.Entry<Double, Heritage>> optional = storage.entries().stream().findAny();
                if (optional.isPresent()) {
                    Map.Entry<Double, Heritage> current = optional.get();
                    String printable = current.getValue().getPrintableCode();
                    logger.info("Current best entry is {}... ({}), distance of {}",
                            printable.substring(0, 7),
                            printable.length(),
                            current.getKey()
                    );
                    logger.info("Current best lastResult: {}", current.getValue().getPrintableResult());
                }
                FileStorage.store(HeritagePool.class, storage);
            }
            pos++;
            latch.countDown();
        }
    }

    public static void rate(Heritage heritage, Double rating) {
        cache.put(rating, heritage);
    }
}
