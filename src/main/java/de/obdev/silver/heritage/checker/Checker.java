package de.obdev.silver.heritage.checker;

import de.obdev.silver.heritage.runner.Result;

import java.util.List;

public interface Checker {

    public void check(List<Result> results);

}
