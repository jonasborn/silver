package de.obdev.silver.heritage.checker;

import de.obdev.silver.heritage.runner.Result;
import smile.math.distance.HammingDistance;

import java.util.List;

public class SimpleChecker implements Checker {
    @Override
    public void check(List<Result> results) {
        if (results != null) {
            for (Result result : results) {
                int[] fin;
                int distance = 0;
                if (result.getResult().length < result.getIoe().getOutput().length) {
                    fin = new int[result.getIoe().getOutput().length];
                    push(fin, result.getResult());
                    for (int i = 0; i < result.getIoe().getOutput().length - result.getResult().length; i++) {
                        fin[i] = 0;
                    }
                    distance = distance + result.getIoe().getOutput().length-result.getResult().length;
                } else {
                    fin = new int[result.getIoe().getOutput().length];
                    System.arraycopy(result.getResult(), 0, fin, 0, fin.length);
                }
                distance = distance + HammingDistance.d(result.getIoe().getOutput(), fin);
                distance = distance + result.getStepps();
//                System.out.println("Distance: " + distance);


                result.getHeritage().rate((double) distance);
            }
        }
    }

    private static int[] push(int[] array, int[] push) {
        int[] longer = new int[array.length + push.length];

        System.arraycopy(array, 0, longer, 0, array.length);
        System.arraycopy(push, 0, longer, array.length, push.length);
        return longer;
    }
}
