package de.obdev.silver.heritage;

import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.nio.charset.Charset;

public class FileStorage {

    static File folder = new File("storage");

    static Gson gson = new Gson();

    static {
        if (!folder.exists()) folder.mkdirs();
        if (folder.isFile()) {
            folder.delete();
            folder.mkdirs();
        }
    }

    public static void store(Class parent, Object o) {
        String fn = parent.getName();
        File target = new File(folder, fn);

        try {
            String json = gson.toJson(o);
            byte[] data = json.getBytes("UTF-8");
            if (target.exists()) target.delete();
            Files.write(data, target);
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }

    }

    public static <T> T read(Class parent, TypeToken<T> token) {
        String fn = parent.getName();
        File target = new File(folder, fn);

        if (!target.exists()) return null;
        if (target.isDirectory()) return null;

        try {
            FileInputStream fis = new FileInputStream(target);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            int r = 0;
            byte[] buffer = new byte[2048];
            while ((r = fis.read(buffer)) != -1) {
                bos.write(buffer, 0, r);
            }
            String json = new String(bos.toByteArray(), "UTF-8");
            return gson.fromJson(json, token.getType());
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
