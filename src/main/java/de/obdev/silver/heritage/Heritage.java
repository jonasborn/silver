package de.obdev.silver.heritage;

import de.obdev.silver.heritage.mutator.Creator;
import de.obdev.silver.heritage.mutator.CreatorPool;
import de.obdev.silver.heritage.mutator.SimpleCreator;
import de.obdev.silver.util.Random;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Heritage implements Serializable {

    Creator lastCreator;

    int length = 8;

    int[] lastResult;

    List<Genome> genomes = new ArrayList<>();

    public Heritage() {
        for (int i = 0; i < length; i++) {
            genomes.add(new Genome());
        }
    }

    public Heritage(int length, int genomeLength) {
        for (int i = 0; i < length; i++) {
            genomes.add(new Genome(genomeLength));
        }
    }

    public Heritage(int length, List<Genome> genomes) {
        this.length = length;
        this.genomes = genomes;
    }

    public int[] getCode() {
        List<Integer> commands = new ArrayList<>();
        for (Genome gen : genomes) {
            for (int command : gen.getCode()) {
                commands.add(command);
            }
        }
        return commands.stream().mapToInt(i -> i).toArray();
    }

    public String getPrintableCode() {
        StringBuilder builder = new StringBuilder();
        for (Genome gen : genomes) {
            for (int command : gen.getCode()) {
                builder.append(command);
            }
        }
        return builder.toString();
    }

    public Heritage mutate() {
        genomes.forEach(Genome::mutate);
        return this;
    }

    public Heritage cross(Heritage heritage) {
        List<Genome> genomes = new ArrayList<>();
        boolean local = Random.decide(0.5);

        List<Genome> source = (local) ? this.genomes : heritage.genomes;
        List<Genome> target = (local) ? heritage.genomes : this.genomes;
        int targetSize = target.size();

        int p = 0;
        for (Genome g : source) {
            if (p >= targetSize) break;
            genomes.add(g.cross(target.get(p)));
        }


        return new Heritage(genomes.size(), genomes );
    }

    public void rate(Double rating) {
        HeritagePool.rate(this, rating);
        if (this.lastCreator != null) {
            CreatorPool.rate(lastCreator, rating);
        }
    }

    public int[] getLastResult() {
        return lastResult;
    }

    public Heritage setLastResult(int[] lastResult) {
        this.lastResult = lastResult;
        return this;
    }

    public Heritage setLastCreator(SimpleCreator lastCreator) {
        this.lastCreator = lastCreator;
        return this;
    }

    public String getPrintableResult() {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < lastResult.length; i++) {
            builder.append(lastResult[i]);
        }
        return builder.toString();
    }
}
