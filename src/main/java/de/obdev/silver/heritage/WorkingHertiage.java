package de.obdev.silver.heritage;

import java.util.List;

public class WorkingHertiage extends Heritage {

    int[] bfk;

    public WorkingHertiage() {
        super();
        bfk = fromBrainfuck("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.");
    }

    public WorkingHertiage(int length, int genomeLength) {
        super(length, genomeLength);
        bfk = fromBrainfuck("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.");
    }

    public WorkingHertiage(int length, List<Genome> genomes) {
        super(length, genomes);
        bfk = fromBrainfuck("++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.");
    }

    @Override
    public int[] getCode() {
        return bfk;
    }

    public static int[] fromBrainfuck(String s) {
        String dict = "><+-.,[]";
        int[] res = new int[s.length()];
        int i = 0;
        for (char c : s.toCharArray()) {
            res[i] = dict.indexOf(c) + 1;
            i++;
        }
        return res;
    }
}
