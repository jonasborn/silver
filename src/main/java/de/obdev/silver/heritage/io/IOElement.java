package de.obdev.silver.heritage.io;

public class IOElement {

    int[] input;
    int[] output;

    public IOElement(int[] input, int[] output) {
        this.input = input;
        this.output = output;
    }

    public int[] getInput() {
        return input;
    }

    public int[] getOutput() {
        return output;
    }

    public IOElement setInput(int[] input) {
        this.input = input;
        return this;
    }

    public IOElement setOutput(int[] output) {
        this.output = output;
        return this;
    }
}
