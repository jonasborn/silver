package de.obdev.silver.heritage.io;

import com.google.gson.reflect.TypeToken;
import de.obdev.silver.heritage.FileStorage;
import de.obdev.silver.util.Random;

import java.util.ArrayList;
import java.util.List;

public class IOManager implements Runnable {

    static List<IOElement> storage = new ArrayList<>();

    static {
        new Thread(new IOManager()).start();
    }

    static {
        try {
            TypeToken<List<IOElement>> token = new TypeToken<List<IOElement>>() {};
            List<IOElement> st = FileStorage.read(IOManager.class, token);
            if (st != null) storage = st;
        } catch (Exception ignored) {

        }
    }

    public static IOElement next() {
        return storage.get(Random.rand(0, storage.size()));
    }

    public static void add(IOElement element) {
        storage.add(element);
    }

    @Override
    public void run() {
        System.out.println("IOManager started");
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            FileStorage.store(IOManager.class, storage);
        }
    }

    public static List<IOElement> create(Integer size) {
        List<IOElement> elements = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            elements.add(next());
        }
        return elements;
    }
}