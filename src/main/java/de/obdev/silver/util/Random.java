package de.obdev.silver.util;

import java.util.concurrent.ThreadLocalRandom;

public class Random {

    public static int rand(int origin, int bound) {
        return ThreadLocalRandom.current().nextInt(origin, bound);
    }

    public static double rand(double origin, double bound) {
        return ThreadLocalRandom.current().nextDouble(origin, bound);
    }


    public static boolean decide(double bias) {
        double c = ThreadLocalRandom.current().nextDouble(1.0);
        return c <= bias;
    }

}
